const chai = require('chai');
const supertest = require('supertest');
const baseRoute = require('./baseRoute');
const express = require('express');
const expect = chai.expect;

describe('Base Route Happiness', () => {
 it('expect to get a hello', (done) => {
    const app = express();
    app.get('/', baseRoute);
    supertest(app)
      .get('/')
      .expect(200)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.text).to.equal('hello');
          done();
        }
      })
 });
});
