const express = require('express');
const app = express();
const baseRoute = require('./baseRoute')

app.get("/", baseRoute);

app.listen(8080, () => console.log('listening...'));
